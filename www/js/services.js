angular.module('starter.services', [])

.factory('getStatisticsByAge', function($http){
	
	var getData = function(age, year){
		
		return $http({
		
			method:'POST',
			url : 'http://api.scb.se/OV0104/v1/doris/sv/ssd/START/BE/BE0101/BE0101A/BefolkningNy', 
			
			data : {	  "query": [
				    {
				      "code": "Alder",
				      "selection": {
				        "filter": "vs:Ålder1årA",
				        "values": [
				          age
				        ]
				      }
				    },
				    {
				      "code": "ContentsCode",
				      "selection": {
				        "filter": "item",
				        "values": [
				          "BE0101N1"
				        ]
				      }
				    },
				    {
				      "code": "Tid",
				      "selection": {
				        "filter": "item",
				        "values": [
				          year
				        ]
				      }
				    }
				  ],
				  "response": {
				    "format": "json"
				  }
			}	
			
		}).then(function(result){
			return result.data;
		});
		
	};
	
	return {
		getData : getData
	};
	
})

.factory('getAllPopulationByYear', function($http){
	
	var getData = function(year){
		
		return $http({
		
			method:'POST',
			url : 'http://api.scb.se/OV0104/v1/doris/sv/ssd/START/BE/BE0101/BE0101A/BefolkningNy', 
			
			data : {
				  "query": [
				            {
				              "code": "Region",
				              "selection": {
				                "filter": "vs:RegionRiket99",
				                "values": [
				                  "00"
				                ]
				              }
				            },
				            {
				              "code": "ContentsCode",
				              "selection": {
				                "filter": "item",
				                "values": [
				                  "BE0101N1"
				                ]
				              }
				            },
				            {
				              "code": "Tid",
				              "selection": {
				                "filter": "item",
				                "values": [
				                  year
				                ]
				              }
				            }
				          ],
				          "response": {
				            "format": "json"
				          }
				        }
			
		}).then(function(result){
			return result.data;
		});
		
	};
	
	return {
		getData : getData
	};
	
})



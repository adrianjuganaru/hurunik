angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('UserInfoViewController', function($scope, $state, getStatisticsByAge, getAllPopulationByYear) {
	$scope.age={};
	$scope.year={};

	$scope.ages=[
	             {id:1, value:1},
	             {id:2, value:2},
	             {id:3, value:3},
	             {id:4, value:4},
	             {id:5, value:5},
	             {id:6, value:6},
	             {id:7, value:7},
	             {id:8, value:8},
	             {id:9, value:9},
	             {id:10, value:10},
	             {id:11, value:11},
	             {id:12, value:12},
	             {id:13, value:13},
	             {id:14, value:14},
	             {id:15, value:15},
	             {id:16, value:16},
	             {id:17, value:17},
	             {id:18, value:18},
	             {id:19, value:19},
	             {id:20, value:20},
	             {id:21, value:21},
	             {id:22, value:22},
	             {id:23, value:23},
	             {id:24, value:24},
	             {id:25, value:25},
	             {id:26, value:26},
	             {id:27, value:27},
	             {id:28, value:28},
	             {id:29, value:29},
	             {id:30, value:30},
	             {id:31, value:31},
	             {id:32, value:32},
	             {id:33, value:33},
	             {id:34, value:34},
	             {id:35, value:35},
	             {id:36, value:36},
	             {id:37, value:37},
	             {id:38, value:38},
	             {id:39, value:39},
	             {id:40, value:40},
	             {id:41, value:41},
	             {id:42, value:42},
	             {id:43, value:43},
	             {id:44, value:44},
	             {id:45, value:45},
	             {id:46, value:46},
	             {id:47, value:47},
	             {id:48, value:48},
	             {id:49, value:49},
	             {id:50, value:50},
	             {id:51, value:51},
	             {id:52, value:52},
	             {id:53, value:53},
	             {id:54, value:54},
	             {id:55, value:55},
	             {id:56, value:56},
	             {id:57, value:57},
	             {id:58, value:58},
	             {id:59, value:59},
	             {id:60, value:60},
	             {id:61, value:61},
	             {id:62, value:62},
	             {id:63, value:63},
	             {id:64, value:64},
	             {id:65, value:65},
	             {id:66, value:66},
	             {id:67, value:67},
	             {id:68, value:68},
	             {id:69, value:69},
	             {id:70, value:70},
	             {id:71, value:71},
	             {id:72, value:72},
	             {id:73, value:73},
	             {id:74, value:74},
	             {id:75, value:75},
	             {id:76, value:76},
	             {id:77, value:77},
	             {id:78, value:78},
	             {id:79, value:79},
	             {id:80, value:80},
	             {id:81, value:81},
	             {id:82, value:82},
	             {id:83, value:83},
	             {id:84, value:84},
	             {id:85, value:85},
	             {id:86, value:86},
	             {id:87, value:87},
	             {id:88, value:88},
	             {id:89, value:89},
	             {id:90, value:90},
	             {id:91, value:91},
	             {id:92, value:92},
	             {id:93, value:93},
	             {id:94, value:94},
	             {id:95, value:95},
	             {id:96, value:96},
	             {id:97, value:97},
	             {id:98, value:98},
	             {id:99, value:99},
	             {id:100, value:"100+"}
	             
	             
	             ];
	$scope.years=[
	             {id:1, value:1968},
	             {id:2, value:1969},
	             {id:3, value:1970},
	             {id:4, value:1971},
	             {id:5, value:1972},
	             {id:6, value:1973},
	             {id:7, value:1974},
	             {id:8, value:1975},
	             {id:9, value:1976},
	             {id:10, value:1977},
	             {id:11, value:1978},
	             {id:12, value:1979},
	             {id:13, value:1980},
	             {id:14, value:1981},
	             {id:15, value:1982},
	             {id:16, value:1983},
	             {id:17, value:1984},
	             {id:18, value:1985},
	             {id:19, value:1986},
	             {id:20, value:1987},
	             {id:21, value:1988},
	             {id:22, value:1989},
	             {id:23, value:1990},
	             {id:24, value:1991},
	             {id:25, value:1992},
	             {id:26, value:1993},
	             {id:27, value:1994},
	             {id:28, value:1995},
	             {id:29, value:1996},
	             {id:30, value:1997},
	             {id:31, value:1998},
	             {id:32, value:1999},
	             {id:33, value:2000},
	             {id:34, value:2001},
	             {id:35, value:2002},
	             {id:36, value:2003},
	             {id:37, value:2004},
	             {id:38, value:2005},
	             {id:39, value:2006},
	             {id:40, value:2007},
	             {id:41, value:2008},
	             {id:42, value:2009},
	             {id:43, value:2010},
	             {id:44, value:2011},
	             {id:45, value:2012},
	             {id:46, value:2013},
	             {id:47, value:2014},
	             ];
	$scope.send = function(){

	var getStatisticsByAgeCall = getStatisticsByAge.getData($scope.age.selected.value, $scope.year.selected.value);
	getStatisticsByAgeCall.then(function(result){
	console.log('result is: ' + JSON.stringify(result.data[0].values[0]) + ", key is: " + JSON.stringify(result.data[0].key));
	window.localStorage.setItem('populationByAgeAndYear', result.data[0].values[0]);
	window.localStorage.setItem('currentSelectedYear', $scope.year.selected.value);
	window.localStorage.setItem('currentSelectedAge', $scope.age.selected.value);

		})
		
	var getAllPopulationByYearCall = getAllPopulationByYear.getData($scope.year.selected.value);
	getAllPopulationByYearCall.then(function(result){
	console.log('result is: ' + JSON.stringify(result.data[0].values[0]) + ", key is: " + JSON.stringify(result.data[0].key));
	window.localStorage.setItem('totalPopulationByYear', result.data[0].values[0]);

		})
	

	 console.log("hej hej!");
	 $state.go('app.questionView');
  }
})

.controller('QuestionViewController', function($scope, $state) {


	$scope.currentYear = window.localStorage.getItem('currentSelectedYear');
	
  $scope.datax={
	level:0
  }
  $scope.send = function(){
	
	var populationByAgeAndYear = window.localStorage.getItem('populationByAgeAndYear');
	var totalPopulationByYear = window.localStorage.getItem('totalPopulationByYear');
	var totalPopulationByYear2 = window.localStorage.getItem('totalPopulationByYear2');
	
	var percent = populationByAgeAndYear / totalPopulationByYear * 100;
	window.localStorage.setItem('realPercentShort', percent.toFixed(1));
	window.localStorage.setItem('realPercent', percent);
	window.localStorage.setItem('chosenPercent', $scope.datax.level);
	console.log(percent.toFixed(1));
	
	console.log(percent);
	
	console.log("hej hej!");
	$state.go('app.feedbackView');
  }
  
  })

.controller('FeedbackViewController', function($scope, $state) {
	
	$scope.success = false;
	$scope.prettyClose = false;
	$scope.wayOff = false;
	
	var chosenPercent = window.localStorage.getItem('chosenPercent');
	var realPercent = window.localStorage.getItem('realPercent');
	var realPercentShort = window.localStorage.getItem('realPercentShort');
	$scope.currentSelectedYear = window.localStorage.getItem('currentSelectedYear');
	$scope.currentSelectedAge = window.localStorage.getItem('currentSelectedAge');
	$scope.populationByAge = window.localStorage.getItem('populationByAgeAndYear');
	$scope.totalPopulationByYear = window.localStorage.getItem('totalPopulationByYear');
	$scope.realPercent = realPercent;
	$scope.realPercentShort = realPercentShort;
	$scope.currentGuess = chosenPercent;

	
	if(realPercentShort === chosenPercent){
		$scope.success = true;
		window.localStorage.setItem('success', $scope.success);
		console.log('chosen: ' + chosenPercent);
		console.log('real: ' + realPercent);
		console.log('realShort: ' + realPercentShort);
	} else if (((realPercentShort * 10 + 3) >= chosenPercent * 10) && ((realPercentShort * 10 - 3) <= chosenPercent * 10)){
		$scope.prettyClose = true;
		window.localStorage.setItem('prettyClose', $scope.prettyClose);
		console.log('chosen: ' + chosenPercent);
		console.log('real: ' + realPercent);
		console.log('realShort: ' + realPercentShort);
	} else {
		$scope.wayOff = true;
		window.localStorage.setItem('wayOff', $scope.wayOff);
		console.log('chosen: ' + chosenPercent);
		console.log('real: ' + realPercent);
		console.log('realShort: ' + realPercentShort);
	}
	
	$scope.startNew = function(){
		$scope.success = false;
		window.localStorage.setItem('success', $scope.success);
		$scope.prettyClose = false;
		window.localStorage.setItem('prettyClose', $scope.prettyClose);
		$scope.wayOff = false;
		window.localStorage.setItem('wayOff', $scope.wayOff);
	 console.log("hej hej!");
	 $state.go('app.userInfoView');
  }
	$scope.clearMemory = function(){
		window.localStorage.removeItem('chosenPercent');
		window.localStorage.removeItem('currentSelectedYear');
		window.localStorage.removeItem('populationByAgeAndYear');
		window.localStorage.removeItem('realPercent');
		window.localStorage.removeItem('realPercentShort');
		window.localStorage.removeItem('totalPopulationByYear');
		window.localStorage.removeItem('success');
		window.localStorage.removeItem('prettyClose');
		window.localStorage.removeItem('wayOff');
	}
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
